package com.xebia.techrally.mongodb;

import static org.junit.Assert.*;

import org.bson.types.ObjectId;
import org.junit.Rule;
import org.junit.Test;

import com.mongodb.DBCursor;
import com.xebia.techrally.mongodb.Project.Status;
import com.xebia.util.test.mongodb.TemporaryMongoDb;

/**
 *
 */
public class MongoProjectRepositoryTest {

	@Rule
	public TemporaryMongoDb tempDb = new TemporaryMongoDb("localhost", "techrally-") {
		@Override
		protected void before() throws Throwable {
			super.before();
			repository = new MongoProjectRepository(tempDb.getDB());
		}
	};

	private MongoProjectRepository repository;


	@Test
	public void shoudlFindInsertedProject() {
	    Project p = new Project("test", "test project", Status.PROPOSAL);

	    repository.addProject(p);

	    ObjectId projectId = p.getId();

	    assertNotNull(projectId);

	    Project retrievedProject = repository.getProject(projectId);

	    assertNotNull(retrievedProject);
	    assertEquals(projectId, retrievedProject.getId());
	    assertEquals(Status.PROPOSAL, retrievedProject.getStatus());

	    DBCursor allProjects = repository.getAllProjects();

	    assertNotNull(allProjects);
	    assertTrue(allProjects.hasNext());

	    Project listedProject = (Project) allProjects.next();

	    assertEquals(projectId, listedProject.getId());

	    assertFalse(allProjects.hasNext());
	}

}
