package com.xebia.util.test.mongodb;

import com.mongodb.DB;
import com.mongodb.Mongo;
import org.junit.rules.ExternalResource;

/**
 * @author Iwein Fuld
 */
public class TemporaryMongoDb extends ExternalResource {
	private DB db;
	private final String host;
	private final String dbNamePrefix;

	public TemporaryMongoDb(String host, String dbNamePrefix) {
		this.host = host;
		this.dbNamePrefix = dbNamePrefix;
	}

	@Override
	protected void before() throws Throwable {
		Mongo mongo = new Mongo(host);
		db = mongo.getDB(dbNamePrefix + System.currentTimeMillis());
	}

	@Override
	protected void after() {
		db.dropDatabase();
	}

	public DB getDB(){
		return db;
	}
}
