package com.xebia.techrally.mongodb;

import org.bson.types.ObjectId;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;


/**
 *
 * Tips:
 *
 * The Java driver provides a DBObject interface to save custom objects to the database.
 *
 * For example, suppose one had a class called Tweet that they wanted to save:
 *
 * public class Tweet extends BasicDBObject {
 *     ...
 * }
 *
 * Then you can say:
 *
 * Tweet myTweet = new Tweet();
 *
 * myTweet.put("user", userId);
 * myTweet.put("message", msg);
 * myTweet.put("date", new Date());
 *
 * collection.insert(myTweet);
 *
 * When a document is retrieved from the database, it is automatically converted to a DBObject.
 * To convert it to an instance of your class, use DBCollection.setObjectClass():
 *
 * collection.setObjectClass(Tweet);
 *
 * Tweet myTweet = (Tweet)collection.findOne();
 *
 *
 *
 */
public class MongoProjectRepository implements ProjectRepository {
    private DBCollection projects;

	public MongoProjectRepository(DB db) {
		projects = db.getCollection("items");
		projects.setObjectClass(Project.class);
	}

    @Override
    public void addProject(Project project) {
        projects.save(project);
    }

    @Override
    public void addUser(User project) {
        throw new UnsupportedOperationException("Age left this implementation as an exercise to the reader");
    }

    @Override
    public void setProjectOwner(ObjectId projectId, ObjectId userId) {
        throw new UnsupportedOperationException("Age left this implementation as an exercise to the reader");
    }

    @Override
    public void addProjectMember(ObjectId projectId, ObjectId userId) {
        throw new UnsupportedOperationException("Age left this implementation as an exercise to the reader");
    }

    @Override
    public void addCommentToProject(ObjectId projectId, Comment comment) {
        throw new UnsupportedOperationException("Age left this implementation as an exercise to the reader");
    }

    @Override
    public void addReplyToComment(ObjectId commentId, Comment comment) {
        throw new UnsupportedOperationException("Age left this implementation as an exercise to the reader");
    }

    @Override
    public DBCursor getAllProjects() {
        return projects.find();
    }

    @Override
    public DBCursor getActiveProjects() {
        throw new UnsupportedOperationException("Age left this implementation as an exercise to the reader");
    }

    @Override
    public Project getProject(ObjectId projectId) {
        return (Project) projects.findOne(projectId);
    }

    @Override
    public User getUser(ObjectId userId) {
        throw new UnsupportedOperationException("Age left this implementation as an exercise to the reader");
    }

    @Override
    public DBCursor getCommentsForProject(ObjectId projectId) {
        throw new UnsupportedOperationException("Age left this implementation as an exercise to the reader");
    }





/*
	@Override
	public BlogItem getItem(String id) {
		DBObject result = itemsCollection.findOne(new ObjectId(id));

		return BlogItem.fromJson(result.toString());
	}

	@Override
	public List<BlogItem> getItems(String... tags) {
		List<BlogItem> items = new ArrayList<BlogItem>();
        DBCursor dbCursor = itemsCollection.find();

		while(dbCursor.hasNext()){
			DBObject dbObject = dbCursor.next();
			String json = dbObject.toString();

            items.add(BlogItem.fromJson(json));
		}

		return items;
	}

	@Override
	public String putJsonItem(String json) {
	    DBObject object = (DBObject) JSON.parse(json);

        WriteResult result = itemsCollection.save(object);

		ObjectId id = (ObjectId) object.get("_id");

        return id.toString();
	}
*/
}
