package com.xebia.techrally.mongodb;



public class Project extends ExtendedBasicDBObject {
    private static final long serialVersionUID = 1L;

    private static final String FIELD_DESCRIPTION = "descr";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_STATUS = "status";

    public static enum Status {
        PROPOSAL, ACTIVE, REJECTED, FINISHED
    }

    /** Used for deserialization. */
    public Project() {
    }

    public Project(String name, String shortDescription, Status status) {
        put(FIELD_NAME, name);
        put(FIELD_DESCRIPTION, shortDescription);
        put(FIELD_STATUS, status.name());
    }

    public String getName() {
        return getString(FIELD_NAME);
    }

    public String getShortDescription() {
        return getString(FIELD_DESCRIPTION);
    }

    public Status getStatus() {
        return Status.valueOf(getString(FIELD_STATUS));
    }
}
