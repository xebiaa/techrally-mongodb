package com.xebia.techrally.mongodb;


public class User extends ExtendedBasicDBObject {
    private static final long serialVersionUID = 1L;


    public User(String fullName) {
        put("name", fullName);
    }
}
