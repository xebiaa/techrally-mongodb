package com.xebia.techrally.mongodb;

import java.util.Map;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;


public class ExtendedBasicDBObject extends BasicDBObject {
    
    public ExtendedBasicDBObject() {
        super();
    }
    
    public ExtendedBasicDBObject(int size) {
        super(size);
    }
    
    public ExtendedBasicDBObject(Map m) {
        super(m);
    }
    
    public ExtendedBasicDBObject(String key, Object value) {
        super(key, value);
    }

    public ObjectId getId() {
        return (ObjectId) get("_id");
    }
    
}
