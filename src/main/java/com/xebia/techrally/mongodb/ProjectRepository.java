package com.xebia.techrally.mongodb;

import org.bson.types.ObjectId;

import com.mongodb.DBCursor;


/**
 *
 */
public interface ProjectRepository {

    void addProject(Project project);

    void addUser(User project);

    void setProjectOwner(ObjectId projectId, ObjectId userId);
    void addProjectMember(ObjectId projectId, ObjectId userId);

    void addCommentToProject(ObjectId projectId, Comment comment);
    void addReplyToComment(ObjectId commentId, Comment comment);

    DBCursor getAllProjects();
    DBCursor getActiveProjects();

    Project getProject(ObjectId projectId);
    User getUser(ObjectId userId);

    DBCursor getCommentsForProject(ObjectId projectId);


}
