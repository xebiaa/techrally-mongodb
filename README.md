Techrally: MongoDB
==========================

The Domain
----------

The domain looks like this:

<img src="techrally-mongodb/raw/master/docs/domain.png" width="845" height="493">

Your challenge is to design a MongoDB schema for storing this schema and implementing a simple Java repository for it.

Tip: start experimenting in the MongoDB shell before diving into the Java code


The Java Code
-------------

The main classes from the domain are implemented as subclasses ob BasicDBObject from the MongoDB Java driver. There is a very basic ProjectRepository interface with an even more basic implementation and a unit test for the bits that are already working.

Feel free to change any code you find, nothing is set in stone so make it fit your schema and then make it work.


Documentation
--------------

- [The MongoDB basics tutorial](http://www.mongodb.org/display/DOCS/Tutorial)
- [Java driver API](http://api.mongodb.org/java/2.3/index.html)
- [MongoDB data types in Java](http://www.mongodb.org/display/DOCS/Java+Types)
- [MongoDB Java Language Center](http://www.mongodb.org/display/DOCS/Java+Language+Center)
- [Using DBObject Tip](http://www.mongodb.org/display/DOCS/Java+-+Saving+Objects+Using+DBObject)


